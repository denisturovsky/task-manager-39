package ru.tsc.denisturovsky.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id, date_begin, date_end) " +
            "VALUES (#{task.id}, #{task.name}, #{task.created}, #{task.description}, #{task.userId}, #{task.status}, " +
            "#{task.projectId}, #{task.dateBegin}, #{task.dateEnd})")
    void add(@NotNull @Param("task") Task task);

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id, date_begin, date_end) " +
            "VALUES (#{task.id}, #{task.name}, #{task.created}, #{task.description}, #{userId}, #{task.status}, " +
            "#{task.projectId}, #{task.dateBegin}, #{task.dateEnd})")
    void addWithUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("task") Task task
    );

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Task> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Task> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable Task findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable Task findOneByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{task.userId} AND id = #{task.id}")
    void remove(@NotNull @Param("task") Task task);

    @Update("UPDATE tm_task SET name = #{task.name}, created = #{task.created}, description = #{task.description}, " +
            "user_id = #{task.userId}, status = #{task.status}, project_id = #{task.projectId}, " +
            "date_begin = #{task.dateBegin}, date_end = #{task.dateEnd} WHERE id = #{task.id}")
    void update(@NotNull @Param("task") Task task);

}